/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.restservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author truong
 */
@RestController
@RequestMapping("/api/v1")
public class FeedbackController {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @CrossOrigin
    @GetMapping("/feedbacks")
    public List<Feedback> getAllFeedbacks() {
        return feedbackRepository.findAll();
    }

    @CrossOrigin
    @GetMapping("/feedbacks/{id}")
    public ResponseEntity<Feedback> getFeedbackById(@PathVariable(value = "id") Long feedbackId)
            throws ResourceNotFoundException {
        Feedback feedback = feedbackRepository.findById(feedbackId)
                .orElseThrow(() -> new ResourceNotFoundException("Feedback not found for this id :: " + feedbackId));
        return ResponseEntity.ok().body(feedback);
    }

    @CrossOrigin
    @PostMapping("/feedbacks")
    public Feedback createFeedback(@Valid @RequestBody Feedback feedback) {
        return feedbackRepository.save(feedback);
    }

    @CrossOrigin
    @PutMapping("/feedbacks/{id}")
    public ResponseEntity<Feedback> updateFeedback(@PathVariable(value = "id") Long feedbackId,
            @Valid @RequestBody Feedback feedbackDetails) throws ResourceNotFoundException {
        Feedback feedback = feedbackRepository.findById(feedbackId)
                .orElseThrow(() -> new ResourceNotFoundException("Feedback not found for this id :: " + feedbackId));
        feedback.setTitle(feedbackDetails.getTitle());
        feedback.setClassCourseID(feedbackDetails.getClassCourseID());
        feedback.setMessage(feedbackDetails.getMessage());
        feedback.setUserid(feedbackDetails.getUserid());
        feedback.setEmail(feedbackDetails.getEmail());
        final Feedback updatedFeedback = feedbackRepository.save(feedback);
        return ResponseEntity.ok(updatedFeedback);
    }

    @CrossOrigin
    @DeleteMapping("/feedbacks/{id}")
    public Map<String, Boolean> deleteFeedback(@PathVariable(value = "id") Long feedbackId)
            throws ResourceNotFoundException {
        Feedback feedback = feedbackRepository.findById(feedbackId)
                .orElseThrow(() -> new ResourceNotFoundException("Feedback not found for this id :: " + feedbackId));

        feedbackRepository.delete(feedback);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
