/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.restservice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author truong
 */
@Entity
@Table(name = "tblfeedback")
public class Feedback {

    private Long fbackid;
    private String title;
    private int classcourseid;
    private String message;
    private String userid;
    private String email;

    public Feedback(Long fbackid, String title, int classcourseid, String message, String userid, String email) {
        this.fbackid = fbackid;
        this.title = title;
        this.classcourseid = classcourseid;
        this.message = message;
        this.userid = userid;
        this.email = email;
    }

    public Feedback() {
    }

    

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getfbackid() {
        return fbackid;
    }

    public void setfbackid(Long fbackid) {
        this.fbackid = fbackid;
    }

    @Column(name = "title", nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "classcourseid", nullable = false)
    public int getClassCourseID() {
        return classcourseid;
    }

    public void setClassCourseID(int classcourseid) {
        this.classcourseid = classcourseid;
    }

    @Column(name = "message", nullable = false)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Column(name = "userid", nullable = false)
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
@Column(name = "email", nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Feedback{" + "fbackid=" + fbackid + ", title=" + title + ", classcourseid=" + classcourseid + ", message=" + message + '}';
    }
}
