var countdown;

startCountdown(3600);

function startCountdown(time) {
  var count = time;
  countdown = setInterval(timer, 1000);
  document.getElementById("clock").innerHTML = output(count);

  function timer() {
    count -= 1;
    if (count == -1) {
      clearInterval(countdown);
      startCountdown(time);
      return;
    }

    document.getElementById("clock").innerHTML = output(count);
  }

  function output(formattime) {
    var sec_num = parseInt(formattime, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - hours * 3600) / 60);
    var seconds = sec_num - hours * 3600 - minutes * 60;

    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return hours + ":" + minutes + ":" + seconds;
  }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


