
$(document).ready(function () {
    fetch('http://localhost:8080/api/v1/feedbacks', {
        method: 'get',
    })
        .then(response => response.json())
        .then(data => {
            $('#datatables').DataTable({
                ressponsive: true,
                data: data,
                columns: [
                    {
                        title: "Feedback ID",
                        data: "fbackid",
                        className: "text-center"
                    },
                    {
                        title: "Student ID",
                        data: "userid",
                        className: "text-center"
                    },
                    {
                        title: "Email",
                        data: "email",
                        className: "text-center"
                    },
                    {
                        title: "Title",
                        data: "title",
                        className: "text-center"
                    },
                    {
                        title: "Class Course ID",
                        data: "classCourseID",
                        className: "text-center"
                    },

                    {
                        title: "Message ",
                        data: "message",
                        className: "text-center"
                    }
                ]
            });
        });


});