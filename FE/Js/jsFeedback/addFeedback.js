$(document).ready(function () {
    function validatedData() {
        let title = $("#title").val()
        let message = $("#message").val()
        let uid = $("#uid").val()
        let email = $("#email").val()
        let classCourseID = parseInt($("#course").val())
        
        if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email) === false) {
            return {
                "status": false,
                "data": "invalid email!"
            }
        }
        if (message.trim() === '') {
            return {
                "status": false,
                "data": "message cannot be empty!"
            }
        }
        return {
            "status": true,
            "data": JSON.stringify({
                "title": title,
                "message": message,
                "userid": uid,
                "email": email,
                "classCourseID": classCourseID
            })};
    }

    $("#fb").submit(function (event) {
        event.preventDefault()
        data = validatedData()
        if (data.status === false) {
            alert(data.data)
            return
        }
        fetch('http://localhost:8080/api/v1/feedbacks', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data.data
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Error');
                }
            })
            .then(data => {
                console.log(data)
                alert("Gửi Feedback thành công")
                location.reload()
            })
            .catch(function (error) {
                console.log(error);
                alert(error)
            })

    });
});
